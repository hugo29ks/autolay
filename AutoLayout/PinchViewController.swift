//
//  PinchViewController.swift
//  AutoLayout
//
//  Created by Hugo Montero on 27/6/18.
//  Copyright © 2018 Hugo Montero. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    @IBOutlet weak var squareView: UIView!
    @IBOutlet weak var heightC: NSLayoutConstraint!
    @IBOutlet weak var widthC: NSLayoutConstraint!
    
    // CGFloat: esta optimizado para coordenadas
    var originalHeight:CGFloat = 0
    var originalWidth:CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        originalHeight = heightC.constant
        originalWidth = widthC.constant
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func pinchGestureActivated(_ sender: Any) {
        let pich = sender as! UIPinchGestureRecognizer
        //print("scals: \(pich.scale)")
        //print("velocity: \(pich.velocity)")
        
        heightC.constant = originalHeight * pich.scale
        widthC.constant = originalWidth * pich.scale
        
        if pich.state == .ended{
            UIView.animate(withDuration: 0.5,
                           delay: 0.5,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            self.heightC.constant = self.originalHeight
                            self.widthC.constant = self.originalWidth
            }, completion: { (finished) -> Void in
                // ....
            })
 
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func longPressActivated(_ sender: Any) {
        squareView.backgroundColor = .red
    }
    
    
    @IBAction func rotationGestureActivated(_ sender: Any) {
        let rotationGesture = sender as! UIRotationGestureRecognizer
        //no hace falta xq ya agregamos de la paleta
        //squareView.addGestureRecognizer(rotationGesture)
        
        rotationGesture.view?.transform = (rotationGesture.view?.transform)!.rotated(by: rotationGesture.rotation)
        rotationGesture.rotation = 0
        
        if rotationGesture.state == .ended{
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: .curveEaseIn, animations: {
                rotationGesture.view?.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))
            }, completion: { _ in
                rotationGesture.rotation = 0
            })
        }
    }
    
    
}
